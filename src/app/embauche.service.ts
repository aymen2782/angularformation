import { Injectable } from '@angular/core';
import {Personne} from "./model/Personne";

@Injectable()
export class EmbaucheService {

  embauches : Personne[];
  constructor() {
    this.embauches = [];
  }
  getEmbauches(){
    return this.embauches;
  }

  addEmbauche(personne : Personne){
    this.embauches.push(personne);
    console.log(this.embauches);
  }

  removeEmbauch(personne : Personne){
    var index = this.embauches.indexOf(personne, 0);
    if (index > -1) {
      this.embauches.splice(index, 1);
    }
  }

}
