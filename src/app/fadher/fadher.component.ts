import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fadher',
  templateUrl: './fadher.component.html',
  styleUrls: ['./fadher.component.css']
})
export class FadherComponent implements OnInit {
  mySonData : string;
  fatherColor : string;
  constructor() { }

  ngOnInit() {
    this.fatherColor="red";
  }

  changeCouleur(newColor){
    this.fatherColor=newColor;
  }

  getSonData(event){
    this.mySonData = event.sonData;
  }
}
