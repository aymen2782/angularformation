import { Component, OnInit } from '@angular/core';
import {LoggerService} from "../logger.service";

@Component({
  selector: 'app-compo1',
  templateUrl: './compo1.component.html',
  styleUrls: ['./compo1.component.css'],
  providers: [LoggerService],
})
export class Compo1Component implements OnInit {

  constructor(private loggerService:LoggerService) { }

  ngOnInit() {
  }
  logger(msg:string){
    this.loggerService.loger(msg);
    console.log('In Compo 1');
    console.log(this.loggerService.getLogs());
  }

}
