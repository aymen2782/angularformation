import { Component, OnInit } from '@angular/core';
import {Personne} from "../model/Personne";
import {EmbaucheService} from "../embauche.service";

@Component({
  selector: 'app-embauche',
  templateUrl: './embauche.component.html',
  styleUrls: ['./embauche.component.css']
})
export class EmbaucheComponent implements OnInit {
  embauches : Personne [];

  constructor(private embaucheService:EmbaucheService) {
    this.embauches = embaucheService.getEmbauches();
  }
  ngOnInit() {
  }

}
