import { Injectable } from '@angular/core';
import {Personne} from "./model/Personne";

@Injectable()
export class CvService {

  personnes : Personne[] ;
  constructor() {
    this.personnes=[new Personne("Sellaouti", "Aymen", 35, "assets/images/as.jpg"),
    new Personne("test", "tes", 37, "assets/images/as.jpg")];
  }
  getPersonnes(){
    return this.personnes;
  }



}
