import { Injectable } from '@angular/core';
import {Todo} from "./model/Todo";

@Injectable()
export class TodoService {

  private todos : Todo[];

  constructor() {
    this.todos=[new Todo('sevice' ,'finaliser Tp Service'), new Todo('http','Entamer TP HTTP' )];
  }

  public getTodos(){
    return this.todos;
  }

  public addTodo(title : string, desciption:string){
    this.todos.push(new Todo(title,desciption));
  }

  public removeTodo(todo:Todo){
    var index = this.todos.indexOf(todo, 0);
    if (index > -1) {
      this.todos.splice(index, 1);
    }
  }
}
