import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cv-card',
  templateUrl: './cv-card.component.html',
  styleUrls: ['./cv-card.component.css']
})
export class CvCardComponent implements OnInit {
  name : string;
  firstName : string;
  slogan : string;
  path : string;
  job : string;
  constructor() { }

  ngOnInit() {
    this.name = "Sellaouti";
    this.firstName = "Aymen";
    this.slogan = "Tant qu'il y a de la vie il y a de l'espoire";
    this.path = "assets/images/as.jpg"
    this.job = "Enseignant Universitaire"
  }

}
