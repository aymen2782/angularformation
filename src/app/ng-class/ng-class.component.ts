import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styleUrls: ['./ng-class.component.css']
})
export class NgClassComponent implements OnInit {
  classObject : Object;
  team1 : boolean;
  team2 : boolean;
  team3 : boolean;
  constructor() { }

  ngOnInit() {

    this.team1 = true;
    this.team2 = false;
    this.team3 = false;
    this.classObject = {
      'c1':this.team1,
      'c2':this.team2,
      'c3':this.team3,
    }
  }
  modifyTeam(team){
   if(team=='c1'){
     this.classObject['c1']=true;
     this.classObject['c2']=false;
     this.classObject['c3']=false;
   }else if(team=='c2'){
     this.classObject['c1']=false;
     this.classObject['c2']=true;
     this.classObject['c3']=false;
   }else{
     this.classObject['c1']=false;
     this.classObject['c2']=false;
     this.classObject['c3']=true;
   }
  }
}
