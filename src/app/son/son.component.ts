import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-son',
  templateUrl: './son.component.html',
  styleUrls: ['./son.component.css']
})
export class SonComponent implements OnInit {

  @Input() color="green";
  constructor() { }

  @Output() sendData = new EventEmitter();
  ngOnInit() {
  }

  envoyerData(dataToSend){
    this.sendData.emit(
      {
        sonData : dataToSend
      }
    )
  }
}
