import { Component, OnInit } from '@angular/core';
import {Personne} from "../model/Personne";

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  selectedPersonne : Personne;
  constructor() { }

  ngOnInit() {
  }

  getSelectedPersonne(event){
    this.selectedPersonne = event.selectedPersonne;
    console.log('Dans Cv Component');
    console.log(this.selectedPersonne);
  }

}
