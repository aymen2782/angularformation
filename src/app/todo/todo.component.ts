import { Component, OnInit } from '@angular/core';
import {TodoService} from "../todo.service";
import {Todo} from "../model/Todo";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  todos : Todo[];
  title : string;
  description : string;
  valide : Boolean;
  constructor(private todoService:TodoService) {
    this.todos = todoService.getTodos();
    this.valide = false;
    this.description = "";
    this.title = "";
  }

  deleteTodo(todo : Todo){
    this.todoService.removeTodo(todo);
    this.todos = this.todoService.getTodos();
  }

  addToDo(titre : string, description :string){
    this.todoService.addTodo(titre,description);
    this.todos = this.todoService.getTodos();
  }
  ngOnInit() {
  }

  isValide(){
    this.valide = (this.description.length>0) &&(this.title.length>0);
  }

}
