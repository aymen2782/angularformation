import {RouterModule, Route} from "@angular/router";
import {CvComponent} from "./cv/cv.component";
import {CvCardComponent} from "./cv-card/cv-card.component";
import {NgStyleComponent} from "./ng-style/ng-style.component";
import {ColorChangeComponent} from "./color-change/color-change.component";

const APP_ROUTES : Route[] = [
  {path: '',component: CvComponent},
  {path: 'cvcard',component: CvCardComponent},
  {path: 'color/:color',component: ColorChangeComponent},
  {path: '**',component: NgStyleComponent},
];

export const routing = RouterModule.forRoot(APP_ROUTES);
