import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appExeg]'
})
export class ExegDirective {

  constructor() { }
  @HostBinding('style.color') color = "blue";
  @HostBinding('style.font-size') fSize = "25px";

  @HostListener('mouseover') exeg(){
    (this.color=="blue")?this.color="red":this.color="blue";
    (this.fSize=="25px")?this.fSize="50px":this.fSize="25px";
  }
}
