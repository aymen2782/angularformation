import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  templateUrl: './ng-style.component.html',
  styleUrls: ['./ng-style.component.css']
})
export class NgStyleComponent implements OnInit {

  myFont : string;

  size : any;

  couleur : string;

  constructor() { }

  ngOnInit() {
    this.myFont="verdana";
    this.size = "10px"
    this.couleur = "red";
  }

  changerCouleur(couleur){
    this.couleur = couleur;
  }
  changerPolice(police){
    console.log(police);
    this.myFont = police;
  }

  changerTaille(taille){
    this.size = taille+"px";
  }

}
