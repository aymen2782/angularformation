import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {__param} from "../../../../forma - partie sans service dans le cv/node_modules/tslib";

@Component({
  selector: 'app-color-change',
  templateUrl: './color-change.component.html',
  styleUrls: ['./color-change.component.css']
})
export class ColorChangeComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute) {
    activatedRoute.params.subscribe(
      params =>{
        this.color = params['color'];
        console.log(this.color);
      }
    )
  }
  color : string;
  ngOnInit() {

  }

  getCouleur(){
    return this.color;
  }

  modifyColor(newColor){
    this.color=newColor;
  }
}
