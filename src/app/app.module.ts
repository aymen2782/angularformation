import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Router} from "@angular/router";

import { AppComponent } from './app.component';
import { PremierComponent } from './premier/premier.component';
import { FilsComponent } from './fils/fils.component';
import { BindingComponent } from './binding/binding.component';
import { ColorChangeComponent } from './color-change/color-change.component';
import { CvCardComponent } from './cv-card/cv-card.component';
import { FadherComponent } from './fadher/fadher.component';
import { SonComponent } from './son/son.component';
import { CvComponent } from './cv/cv.component';
import { ListeComponent } from './liste/liste.component';
import { ItemComponent } from './item/item.component';
import { DetailComponent } from './detail/detail.component';
import { NgStyleComponent } from './ng-style/ng-style.component';
import { NgClassComponent } from './ng-class/ng-class.component';
import { ExergueDirective } from './exergue.directive';
import { ExegDirective } from './exeg.directive';
import {PersonneService} from "./personne.service";
import { TodoComponent } from './todo/todo.component';
import {TodoService} from "./todo.service";
import { Compo1Component } from './compo1/compo1.component';
import { Compo2Component } from './compo2/compo2.component';
import {LoggerService} from "./logger.service";
import { EmbaucheComponent } from './embauche/embauche.component';
import {CvService} from "./cv.service";
import {EmbaucheService} from "./embauche.service";
import {routing} from "./app.routing";
import { HeaderComponent } from './header/header.component';
import { FirstHttpComponent } from './first-http/first-http.component';
import {PostsService} from "./posts.service";
import { PostsComponent } from './posts/posts.component';

@NgModule({
  declarations: [
    AppComponent,
    PremierComponent,
    FilsComponent,
    BindingComponent,
    ColorChangeComponent,
    CvCardComponent,
    FadherComponent,
    SonComponent,
    CvComponent,
    ListeComponent,
    ItemComponent,
    DetailComponent,
    NgStyleComponent,
    NgClassComponent,
    ExergueDirective,
    ExegDirective,
    TodoComponent,
    Compo1Component,
    Compo2Component,
    EmbaucheComponent,
    HeaderComponent,
    FirstHttpComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [
    EmbaucheService,
    PersonneService,
    TodoService,
    LoggerService,
    CvService,
    PostsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
