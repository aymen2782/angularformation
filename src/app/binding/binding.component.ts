import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  constructor() { }
  nom : string;
  prenom : string;
  ngOnInit() {
    this.nom="Sellaouti";
    this.prenom="Aymen";
  }
  changeName(newName){
    this.nom=newName;
  }
  getName(){
    return this.nom;
  }

  modifier(valeur){
    this.nom = "new "+valeur;
  }

}
