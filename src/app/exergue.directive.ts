import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appExergue]'
})
export class ExergueDirective {

  @HostBinding('style.color') color = "red";
  @HostBinding('style.borderColor') bg = "blue";
  mesCouleurs = [
    'RED','ORANGE','YELLOW','GREEN','BLUE','INDIGO','VIOLET',
    'gold', 'darkgoldenrod', 'lightgray', 'rebeccapurple'
  ];
  constructor() { }

  @HostListener('keydown') newColor() {
    const colorPick = Math.floor(Math.random() * this.mesCouleurs.length);
    this.color = this.bg = this.mesCouleurs[colorPick];
  }

}
