import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Personne} from "../model/Personne";
import {CvService} from "../cv.service";

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.css']
})
export class ListeComponent implements OnInit {


  personnes : Personne[] ;
  selectedPersonne : Personne;
  @Output() selectPersonneEvent = new EventEmitter();
  constructor(private cvService:CvService) { }

  ngOnInit() {
    this.personnes = this.cvService.getPersonnes();
  }

  getSelectedPersonne(event){
    this.selectedPersonne = event.selectedPersonne;
    this.selectPersonneEvent.emit(
      {
        selectedPersonne : this.selectedPersonne
      }
    )
  }

}
