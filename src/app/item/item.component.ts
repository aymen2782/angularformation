import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Personne} from "../model/Personne";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() myPersonne : Personne;
  @Output() selectPersonneEvent = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  SelectPersonne(){
    this.selectPersonneEvent.emit(
      {
        selectedPersonne : this.myPersonne
      }
    )
  }
}
