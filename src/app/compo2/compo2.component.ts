import { Component, OnInit } from '@angular/core';
import {LoggerService} from "../logger.service";


@Component({
  selector: 'app-compo2',
  templateUrl: './compo2.component.html',
  styleUrls: ['./compo2.component.css'],
  providers: [],
})
export class Compo2Component implements OnInit {

  constructor(private loggerService:LoggerService) { }

  ngOnInit() {
  }

  logger(msg:string){
    this.loggerService.loger(msg);
    console.log('In Compo 2');
    console.log(this.loggerService.getLogs());
  }
}
