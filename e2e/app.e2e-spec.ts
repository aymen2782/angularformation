import { FormaPage } from './app.po';

describe('forma App', () => {
  let page: FormaPage;

  beforeEach(() => {
    page = new FormaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
